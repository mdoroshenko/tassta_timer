//
//  ViewController.swift
//  Tassta_Timer
//
//  Created by Максим Дорошенко on 23.11.15.
//  Copyright © 2015 Максим Дорошенко. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    var timerStarted = false;
    var isActive = true;
    
    var suspendedTime: Double = 0.0
    var timeFix: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "pause", name: UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "resume", name: UIApplicationWillEnterForegroundNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startButtonPressed(sender: AnyObject) {
        timeFix = 0.0
        let operation = TimerOperation(timerDelegate: self)
        Queues.Instance.timerQueue.addOperation(operation)
        reverseButtons()
    }
    
    @IBAction func stopButtonPressed(sender: AnyObject) {
        Queues.Instance.timerQueue.cancelAllOperations()
        reverseButtons()
    }
    
    func resume() {
        isActive = true
        timeFix += round((NSDate().timeIntervalSinceReferenceDate - suspendedTime))
        print(round((NSDate().timeIntervalSinceReferenceDate - suspendedTime)))
    }
    
    func pause() {
        isActive = false
        suspendedTime = NSDate().timeIntervalSinceReferenceDate
    }
    
    func reverseButtons() {
        startButton.hidden = !timerStarted
        stopButton.hidden = timerStarted
        timerStarted = !timerStarted
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

}

// MARK: TimerDelegate
extension TimerViewController: TimerDelegate {
    func timerUpdated(newValue: Double) {
        if isActive {
            dispatch_async(dispatch_get_main_queue(), {
                let fullTime = self.timeFix + newValue
                let seconds = Int(fullTime % 60)
                let minutes = Int((fullTime / 60) % 60)
                let hours = Int(fullTime / 3600)
                self.timerLabel.text = "\(hours)h:\(minutes)m:\(seconds)sec"})
        }
    }
}

