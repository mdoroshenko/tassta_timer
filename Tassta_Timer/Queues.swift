//
//  Queues.swift
//  Tassta_Timer
//
//  Created by Максим Дорошенко on 23.11.15.
//  Copyright © 2015 Максим Дорошенко. All rights reserved.
//

import Foundation

class Queues {
    
    static let Instance = Queues()
    
    lazy var timerQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "com.Tassta-Timer.timerQueue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}
