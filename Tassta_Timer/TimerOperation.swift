//
//  TimerOperation.swift
//  Tassta_Timer
//
//  Created by Максим Дорошенко on 23.11.15.
//  Copyright © 2015 Максим Дорошенко. All rights reserved.
//

import UIKit

class TimerOperation: NSOperation {
    
    var curTime: Double = 0.0
    var timerDelegate: TimerDelegate?
    var timer: NSTimer?
    let step = 1.0
    
    convenience init(timerDelegate: TimerDelegate) {
        self.init()
        self.timerDelegate = timerDelegate
        self.timer = NSTimer.scheduledTimerWithTimeInterval(step, target: self, selector: "increaseTimer", userInfo: nil, repeats: true)
    }
    
    override func main() {
        while !cancelled {
        }
        timer?.invalidate()
    }
    
    func increaseTimer() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , {
            self.curTime += self.step
            self.timerDelegate?.timerUpdated(self.curTime)})
    }
}
