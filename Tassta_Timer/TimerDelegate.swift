//
//  TimerDelegate.swift
//  Tassta_Timer
//
//  Created by Максим Дорошенко on 23.11.15.
//  Copyright © 2015 Максим Дорошенко. All rights reserved.
//

protocol TimerDelegate {
    func timerUpdated(newValue: Double)
}
